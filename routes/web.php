<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NewController;
use GuzzleHttp\Middleware;

Route::get('/', [NewController::class, 'index']);
//CRUD - CREATE - CRIAR
Route::get('/events/create', [NewController::class, 'create'])->middleware('auth');
Route::post('/events', [NewController::class, 'store']);

//CRUD - READ - LER ( EXIBIR )
Route::get('/events/{id}', [NewController::class, 'show']);

//CRUD - UPDATE - ATUALIZAR
Route::get('/events/edit/{id}', [NewController::class, 'edit'])->middleware('auth');
Route::put('/events/update/{id}', [NewController::class, 'update'])->middleware('auth');

//CRUD - DELETE - APAGAR
Route::delete('/events/{id}', [NewController::class, 'destroy'])->middleware('auth');



Route::get('/contsct', function () {
    return view('contact');
});

Route::get('/dashboard', [NewController::class, 'dashboard'])->middleware('auth');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('/events/join/{id}', [NewController::class, 'joinEvent'])->middleware('auth');

Route::delete('/events/leave/{id}', [NewController::class, 'leaveEvent'])->middleware('auth');
